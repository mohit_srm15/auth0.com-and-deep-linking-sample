import React, {useState, useEffect} from 'react';
import {View, ScrollView, TouchableOpacity, Platform} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';

import {BoldText} from '../Components/Common/StyledText';
import Color from '../Styles/Color';
import {onShare} from '../Common/Helper';
import {getUser, login, logout} from '../Common/Auth';

function Home() {
  const [userCredentials, setUserCredentials] = useState({});
  const [loginStatus, setLoginStatus] = useState(false);

  const options = {
    message: 'You can open meddoTest app meddoapp://home/',
    url: 'meddoapp://home/',
  };

  useEffect(() => {
    getUserDetails();
  }, [0]);

  async function getUserDetails() {
    let token = await AsyncStorage.getItem('token');
    if (token) {
      let detail = await getUser(token);
      if (detail && detail.email) {
        setUserCredentials(detail);
        setLoginStatus(true);
      }
    }
  }

  async function handleLogin() {
    let credentials = await login();
    setUserCredentials(credentials);
    setLoginStatus(true);
  }

  async function handleLogout() {
    let logoutStatus = await logout();
    if (logoutStatus) {
      setUserCredentials({});
      setLoginStatus(false);
      AsyncStorage.setItem('token', '');
    }
  }

  return (
    <View style={{flex: 1}}>
      <View
        style={{
          flex: 0.1,
          backgroundColor: Color.gray05,
          paddingHorizontal: 10,
        }}>
        <View
          style={{
            flexDirection: 'row',
            flex: 1,
            alignItems: Platform.OS === 'ios' ? 'flex-end' : 'center',
            paddingHorizontal: 5,
          }}>
          <View style={{flex: 0.88}}>
            <BoldText
              style={{
                color: Color.headerColor,
                fontSize: 35,
                letterSpacing: 10,
              }}>
              {loginStatus ? 'Dashboard' : 'meddo'}
            </BoldText>
          </View>
        </View>
      </View>
      <ScrollView style={{flex: 1}}>
        <View style={{margin: 20}}>
          <View>
            {loginStatus ? (
              <TouchableOpacity onPress={() => handleLogout()}>
                <BoldText style={{color: Color.gray04, fontWeight: 'bold'}}>
                  Logout
                </BoldText>
              </TouchableOpacity>
            ) : (
              <TouchableOpacity onPress={() => handleLogin()}>
                <BoldText style={{color: Color.gray04, fontWeight: 'bold'}}>
                  Login
                </BoldText>
              </TouchableOpacity>
            )}
          </View>
          <View style={{marginTop: 20}}>
            <TouchableOpacity onPress={() => onShare(options)}>
              <BoldText style={{color: Color.headerColor, fontWeight: 'bold'}}>
                Share App Link
              </BoldText>
            </TouchableOpacity>
          </View>
        </View>
      </ScrollView>
    </View>
  );
}

export default Home;
