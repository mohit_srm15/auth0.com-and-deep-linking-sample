import {Share} from 'react-native';

export function onShare(options) {
  try {
    let result = Share.share(options);
  } catch (e) {
    console.log(' error ', e);
  }
}
