import axios from 'axios';

import {UTILS} from '../Constants/Utils';
import {auth0} from '../Auth';

export async function getUser(token) {
  let details = await axios
    .get(UTILS.URL, {
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
    })
    .then((result) => {
      if (result && result.data) {
        return result.data;
      }
    })
    .catch((error) => {
      alert(error);
      return error;
    });
  return details;
}

export async function login() {
  let credentials = await auth0.webAuth
    .authorize({scope: 'openid profile email'})
    .then(async (credentials) => {
      return credentials;
    })
    .catch((error) => console.log(error));
  return credentials;
}

export async function logout() {
  let logoutStatus = await auth0.webAuth
    .clearSession({})
    .then((success) => {
      alert('Logged out!');
      return true;
    })
    .catch((error) => {
      console.log('Log out cancelled');
      return false;
    });
  return logoutStatus;
}
