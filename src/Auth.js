import Auth0 from 'react-native-auth0';

import {CONFIG} from './Constants/Auth0Config';

export const auth0 = new Auth0(CONFIG);
