import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';

import Splash from './Screens/Splash';
import Home from './Screens/Home';

const appNavigations = createStackNavigator(
  {
    Splash: {screen: Splash},
    Home: {screen: Home, path: 'home'},
  },
  {
    headerMode: 'none',
  },
);

export const AppContainer = createAppContainer(appNavigations);
