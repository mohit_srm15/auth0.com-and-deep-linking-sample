/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {View} from 'react-native';
import 'react-native-gesture-handler';

import {AppContainer} from './src/Router';

function App() {
  const prefix = 'meddo://meddo/';
  return (
    <View style={{flex: 1}}>
      <AppContainer uriPrefix={prefix} />
    </View>
  );
}

export default App;
